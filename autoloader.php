<?php

spl_autoload_register(function ( $file ) {
    require __DIR__ . '/src/' . str_replace('\\', '/', $file) . '.php';
});