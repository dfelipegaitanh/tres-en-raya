<?php

namespace Connection;


use PDO;
use PDOException;

class Connector
{

    /**
     * @var PDO
     */
    protected $connection;
    /**
     * @var array
     */
    private $credentials;

    function __construct()
    {
        $this->credentials = parse_ini_file(__DIR__ . '/../../configuration/database.ini');
        $this->connect();
    }

    /**
     * @throws PDOException
     */
    function connect()
    {
        try {
            $this->connection = new PDO("mysql:host={$this->credentials['host']};dbname={$this->credentials['dbname']}", $this->credentials['username'], $this->credentials['password']);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    /**
     * @param $query
     * @return bool
     */
    public function exec( $query )
    {
        return (bool)$this->getConnection()
                          ->exec($query);
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }

}