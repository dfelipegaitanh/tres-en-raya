<?php

class VideoStream
{
    private $path = "";
    private $stream = "";
    private $buffer = 102400;
    private $start = -1;
    private $end = -1;

    private $protocol;
    private $host;
    private $port;
    private $uri;
    private $streamLength;

    function __construct( $filePath )
    {
        $this->path = $filePath;
    }

    /**
     * Start streaming video content
     */
    function start()
    {
        $this->open();
        $this->setHeader();
        $this->stream();
        $this->end();
    }

    /**
     * Open stream
     */
    private function open()
    {
        $this->scanUrl();
        $this->getDataUrl();
    }

    private function scanUrl()
    {

        $pos = strpos($this->path, '://');

        $this->protocol = strtolower(substr($this->path, 0, $pos));

        list($pos, $host) = $this->getHost($pos);
        list($this->host, $this->port) = ( strpos($host, ':') ) ? explode(':', $host) : [$host, ( ( $this->protocol == 'https' ) ? 443 : 80 )];

        $this->uri = !substr($this->path, $pos) !== '' ? substr($this->path, $pos) : '/';

    }

    /**
     * @param $pos
     * @return array
     */
    private function getHost( $pos )
    {
        $this->path = substr($this->path, $pos + 3);
        $pos        = ( $pos === false ) ? strlen($this->path) : strpos($this->path, '/');
        $host       = substr($this->path, 0, $pos);
        return [$pos, $host];
    }

    private function getDataUrl()
    {
        $crlf = "\r\n";

        $req      = $this->generateRequest($crlf);
        $response = $this->fetchDataUrl($req);

        $pos = strpos($response, $crlf . $crlf);
        if ( $pos === false ) {
            return ( $response );
        }
        $this->generateTempFile($response, $pos, $crlf);
    }

    /**
     * @param $crlf
     * @return string
     */
    private function generateRequest( $crlf )
    {
        return 'GET ' . $this->uri . ' HTTP/1.0' . $crlf
            . 'Host: ' . $this->host . $crlf
            . $crlf;
    }

    /**
     * @param $req
     * @return string
     */
    private function fetchDataUrl( $req )
    {
        $fp = fsockopen(( $this->protocol == 'https' ? 'ssl://' : '' ) . $this->host, $this->port);
        fwrite($fp, $req);
        $response = "";
        while (is_resource($fp) && $fp && !feof($fp)) {
            $response .= fread($fp, 1024);
        }
        fclose($fp);
        return $response;
    }

    /**
     * @param $response
     * @param $pos
     * @param $crlf
     */
    private function generateTempFile( $response, $pos, $crlf )
    {
        $this->stream = tmpfile();
        fwrite($this->stream, substr($response, $pos + 2 * strlen($crlf)));
        $this->streamLength = filesize(stream_get_meta_data($this->stream)['uri']);
    }

    /**
     * Set proper header to serve the video content
     */
    private function setHeader()
    {
        ob_get_clean();
        header("Content-Type: video/mp4");
        header("Cache-Control: max-age=2592000, public");
        header("Expires: " . gmdate('D, d M Y H:i:s', time() + 2592000) . ' GMT');
        header("Last-Modified: " . gmdate('D, d M Y H:i:s', time() + 2592000 - 1) . ' GMT');
        $this->start = 0;
        $size        = $this->streamLength;
        $this->end   = $size - 1;
        header("Accept-Ranges: 0-" . $this->end);

        ( isset($_SERVER['HTTP_RANGE']) ) ? $this->headers_HTTP_RANGE() : header("Content-Length: " . $size);

    }

    private function headers_HTTP_RANGE()
    {
        $size  = $this->streamLength;
        $c_end = $this->end;

        list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
        if ( strpos($range, ',') !== false ) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header("Content-Range: bytes $this->start-$this->end/$size");
            exit;
        }
        if ( $range == '-' ) {
            $c_start = $size - substr($range, 1);
        } else {
            $range   = explode('-', $range);
            $c_start = $range[0];

            $c_end = ( isset($range[1]) && is_numeric($range[1]) ) ? $range[1] : $c_end;
        }
        $c_end = ( $c_end > $this->end ) ? $this->end : $c_end;
        if ( $c_start > $c_end || $c_start > $size - 1 || $c_end >= $size ) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header("Content-Range: bytes $this->start-$this->end/$size");
            exit;
        }
        $this->start = $c_start;
        $this->end   = $c_end;
        $length      = $this->end - $this->start + 1;
        fseek($this->stream, $this->start);
        header('HTTP/1.1 206 Partial Content');
        header("Content-Length: " . $length);
        header("Content-Range: bytes $this->start-$this->end/" . $size);
    }

    /**
     * perform the streaming of calculated range
     */
    private function stream()
    {

        $i = $this->start;
        set_time_limit(0);
        fseek($this->stream, 0);
        while (!feof($this->stream) && $i <= $this->end) {
            $bytesToRead = $this->getBytesToRead($i);
            echo fread($this->stream, $bytesToRead);
            flush();
            $i += $bytesToRead;
        }

    }

    /**
     * close curretly opened stream
     */
    private function end()
    {
        fclose($this->stream);
        exit;
    }

    /**
     * @param $i
     * @return int
     */
    private function getBytesToRead( $i )
    {
        $bytesToRead = $this->buffer;
        if ( ( $i + $bytesToRead ) > $this->streamLength ) {
            $bytesToRead = $this->end - $i + 1;
        }
        return $bytesToRead;
    }
}

( new VideoStream('http://videoteca.cudi.edu.mx/bitstream/handle/11305/1781/181025_Conferencia_Academia_Ingenieria_Ingreso_Francisco_Niembro.mp3') )->start();
